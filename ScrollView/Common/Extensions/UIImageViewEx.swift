//
//  UIImageViewEx.swift
//  ScrollView
//
//  Created by Pathmazing on 8/5/19.
//  Copyright © 2019 Pathmazing Inc. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    @IBInspectable
    var borderRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
}
