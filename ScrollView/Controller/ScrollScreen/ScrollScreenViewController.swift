//
//  ScrollScreenViewController.swift
//  ScrollView
//
//  Created by Pathmazing on 8/5/19.
//  Copyright © 2019 Pathmazing Inc. All rights reserved.
//

import UIKit

class ScrollScreenViewController: UIViewController {

    private var imageButton = [#imageLiteral(resourceName: "facebook"),#imageLiteral(resourceName: "twitter"),#imageLiteral(resourceName: "linkedin"),#imageLiteral(resourceName: "pinterest"),#imageLiteral(resourceName: "vk"),#imageLiteral(resourceName: "instagram")]
    
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTransparentBackgroundToCollectionView()
        setViewBorderColor()
    }
    
    func setViewBorderColor() {
        let white = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        leftView.layer.borderColor = white.cgColor
        rightView.layer.borderColor = white.cgColor
    }
    
    func setTransparentBackgroundToCollectionView() {
        collectionView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0)
    }
    
}

extension ScrollScreenViewController: UICollectionViewDataSource {
    
}

extension ScrollScreenViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageButton.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SocialMediaScroll", for: indexPath) as! SocialMediaCollectionViewCell
        
        cell.SocialButton.setBackgroundImage(imageButton[indexPath.row], for: .normal)
        
        return cell
    }
}
